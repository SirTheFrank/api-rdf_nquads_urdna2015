#!/usr/bin/env python
# encoding: utf-8
import json
from flask import Flask, request
from pyld import jsonld
from flask_cors import CORS
import json
# requests module necessary too

# for curl/postman:
# https://stackoverflow.com/questions/10434599/get-the-data-received-in-a-flask-request

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return "API Normalizes data using the RDF-nquads-URDNA2015 algorithms. Input JSON, output RDF-nquads"

@app.route('/normalize', methods=['POST'])
def normalize():
    doc = request.json
    if doc == None:
        print("request.json not set")
        return "request.json not set"

    print(doc)

    normalized = jsonld.normalize(
        doc, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})

    return normalized

@app.route('/test')
def test():
    doc = {
      "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
      ],
      "id": "https://example.com/credentials/1872",
      "type": ["VerifiableCredential", "AlumniCredential"],
      "issuer": "https://example.edu/issuers/565049",
      "issuanceDate": "2010-01-01T19:23:24Z",
      "credentialSubject": {
        "id": "did:example:ebfeb1f712ebc6f1c276e12ec21",
        "alumniOf": "Example University"
      }
    }

    normalized = jsonld.normalize(
        doc, {'algorithm': 'URDNA2015', 'format': 'application/n-quads'})
    return normalized

app.run(host='0.0.0.0', port=80)